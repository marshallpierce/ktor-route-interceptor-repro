package org.mpierce.ktor.route

import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.http.HttpMethod
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.RoutingApplicationCall
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.jupiter.api.Test
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.atomic.AtomicInteger
import kotlin.test.assertEquals

class KtorRouteTest {
    @Test
    internal fun rootWildcardRouteClobbersRootInterceptor() {
        withTestApplication({
            routing {
                route("/") {
                    addRouteCounterInterceptor()
                }
                route("/{...}") {
                    handler()
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/")) {
                assertEquals("OK: /{...}", response.content)
            }

            // get empty map instead -- interceptor never runs
            assertEquals(mapOf("/{...}" to 1), counts())
        }
    }

    @Test
    internal fun rootWildcardRouteWorkesWithEmptyRouteInterceptor() {
        withTestApplication({
            routing {
                // note empty string instead of "/"
                route("") {
                    addRouteCounterInterceptor()
                }
                route("/{...}") {
                    handler()
                }
            }
        }) {
            with(handleRequest(HttpMethod.Get, "/")) {
                assertEquals("OK: /{...}", response.content)
            }

            // interceptor runs
            assertEquals(mapOf("/{...}" to 1), counts())
        }
    }

    private val counters = ConcurrentHashMap<String, AtomicInteger>()

    private fun counts() = counters.toMap().mapValues { (_, count) -> count.get() }

    private fun Route.addRouteCounterInterceptor() {
        intercept(ApplicationCallPipeline.Monitoring) {
            val c = call
            // it seems `call` is only a RoutingApplicationCall when the interceptor is added inside `routing`
            val key = if (c is RoutingApplicationCall) {
                // want the route, not the uri, so the counts are per-route even with path params etc
                c.route.toString()
            } else {
                "(not a routing call)"
            }

            val default = AtomicInteger(0)
            (counters.putIfAbsent(key, default) ?: default).incrementAndGet()

            proceed()
        }
    }

    private fun Route.handler() {
        handle {
            call.respond("OK: ${(call as RoutingApplicationCall).route}")
        }
    }
}
