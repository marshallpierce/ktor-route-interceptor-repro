plugins {
    kotlin("jvm") version "1.4.32"
}

repositories {
    mavenCentral()
}

val deps: Map<String, String> by extra {
    mapOf(
        "ktor" to "1.5.2",
        "jupiter" to "5.7.1"
    )
}

dependencies {
    implementation("io.ktor", "ktor-server", deps["ktor"])

    testImplementation("io.ktor", "ktor-server-test-host", deps["ktor"])
    testRuntimeOnly("org.junit.jupiter", "junit-jupiter-engine", "${deps["jupiter"]}")
    testImplementation("org.junit.jupiter", "junit-jupiter-api", "${deps["jupiter"]}")
    testImplementation(kotlin("test-junit5"))
}

tasks {
    test {
        useJUnitPlatform()
    }
}
